import { useState } from 'react';
import './App.css';
import List from './components/list';

function App() {
  const [players, setPlayers] = useState([]);
  const [player, setPlayer] = useState('');
  const [username, setUsername] = useState('');
  const [error, setError] = useState('');
  const [isUpdate, setIsUpdate] = useState(false);
  const [search, setSearch] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    const isFind = players.find(row => row === username);
    if (!isUpdate) {
      if (!isFind) {
        setPlayers([...players, username]);
        setUsername('');
      } else {
        setError('Username already taken!');
        setTimeout(() => {
          setError('')
        }, 1000)
      }
    } else {
      const newPlayers = players.map(row => {
        return row === player ? username : row;
      });

      setPlayer('')
      setPlayers(newPlayers)
      setIsUpdate(false)
      setUsername('')
    }
  }

  const handleDelete = (player) => {
    const newPlayers = players.filter((row) => row !== player);
    setPlayers(newPlayers);
  }

  const handleUpdate = (player) => {
    setIsUpdate(true);
    setPlayer(player);
    setUsername(player);
  }

  const handleSearch = (e) => {
    e.preventDefault();
    const findUsername = players.find(row => row === search);
    console.log(findUsername)
  }

  return (
    <div className='app-container'>
      <form onSubmit={handleSubmit}>
        <label>Form Tambah Player</label><br />
        <input value={username} onChange={(e) => setUsername(e.target.value)} type="text" placeholder='Username' /><br />
        <button type="submit">Add</button>
      </form>
      <br />
      <form onSubmit={handleSearch}>
        <label>Pencarian</label><br />
        <input value={search} onChange={(e) => setSearch(e.target.value)} type="text" placeholder='Cari' /><br />
        <button type='submit'>Search</button>
      </form>
      {error && <p>{error}</p>}
      {players.map((row, index) => (
        <List
          player={row}
          key={index}
          handleUpdate={handleUpdate}
          handleDelete={handleDelete} />
      ))}
    </div>
  );
}

export default App;
