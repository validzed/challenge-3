import './style.css';

const PlayerComponent = ({ player, handleDelete, handleUpdate }) => {
    return (
        <div>
            <p>{player}
                <button onClick={() => handleUpdate(player)}>Update</button>
                <button onClick={() => handleDelete(player)}>Delete</button>
            </p>
        </div>
    )
}

export default PlayerComponent;